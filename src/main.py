from notifier import Notifier
import argparse

parser = argparse.ArgumentParser(description='Notifies bus information.')
parser.add_argument('-b', '--bus', type=int, help='Bus ID')
parser.add_argument('-s', '--stop', type=int, help='Stop ID')

args = parser.parse_args()
print(args.bus)
BUS_NO = 938
CITY_BUS_STOP_ID = "6350786798486033461"

n = Notifier(BUS_NO, CITY_BUS_STOP_ID)
n.run()


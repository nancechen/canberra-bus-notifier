from request import Request
from parser import Parser
import os
from time import sleep

# x minutes before the bus departure to send notification
TIME_LIST = [15, 10, 5]


class Notifier:

    def __init__(self, bus_id, stop_id):
        self.bus_id = bus_id
        self.stop_id = stop_id



    def run(self):
        # if before stating time, sleep until then


        noti_list = [False] * len(TIME_LIST)
        while True: 
            r = Request(self.stop_id)
            response = r.send()

            p = Parser(response)
            bus_departure = p.get_time(self.bus_id)
            # print(response)
            # No bus comming
            if bus_departure is None:
                print("no bus {} coming. Sleeping for 10 minutes".format(self.bus_id))
                sleep(10 * 60)
                continue

            realtime_departure, scheduled_departure = bus_departure

            mins_to_check = min([realtime_departure - t for t in TIME_LIST if realtime_departure > t] + [10])
            if [realtime_departure <= t for t in TIME_LIST] != noti_list:
                noti_list = [realtime_departure <= t for t in TIME_LIST]
                self.notify('Bus information', 
                    'Bus {} is coming in {} mins. Scheduled to come in {} mins.'\
                    .format(self.bus_id, realtime_departure, scheduled_departure))

            if realtime_departure <= 5: 
                noti_list = [False] * len(TIME_LIST)


            print("bus coming in {} minutes. Sleeping for {} minutes".format(realtime_departure, mins_to_check))
            sleep(mins_to_check * 60)
        

    def notify(self, title, message):
        command = 'osascript -e "display notification \\"{}\\" with title \\"{}\\""'.format(message, title)
        os.system(command)
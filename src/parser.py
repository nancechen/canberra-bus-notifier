import json
import datetime

class Parser:
    def __init__(self, response):
        self.response = response


    def get_bus(self, bus_id):
        buses = self.response["body"]["stopDepartures"]
       
        for b in buses: 
            if b["serviceNumber"] == str(bus_id):
                return b
        return None

    def get_time(self, bus_id):
        bus_info = self.get_bus(bus_id)
        if bus_info is not None: 
            rtd = self.convert_time(bus_info["realTimeDeparture"])
            std = self.convert_time(bus_info["scheduledDeparture"])
            rtd_minutes = self.time_until_now(rtd)
            std_minutes = self.time_until_now(std)
            return rtd_minutes, std_minutes

        return None

    def time_until_now(self, time):
        now = datetime.datetime.utcnow()
        diff = time - now
        return (diff.days * 86400 + diff.seconds) // 60

    def convert_time(self,time):
        return datetime.datetime.utcfromtimestamp(time // 1000)

import requests as pyRequest
import json
import datetime

class Request: 
    def __init__(self, stop_id):
        self.stop_id = stop_id
        return

    def send(self):

        current_time = datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%S.000Z")
        current_time = "2018-04-11T00:30:00.000Z"

        url = 'http://www.nxtbus.act.gov.au/departures/Wed%20Apr%2011%202018%2022:11:50%20GMT+1000%20(AEST)'
        # url = 'http://www.nxtbus.act.gov.au/departures/Wed%20Apr%2011%202018%2020:04:30%20GMT+1000%20(AEST)'
        headers = {
            "Host": "www.nxtbus.act.gov.au",
            "Connection": "keep-alive",
            "Content-Length": "214",
            "Pragma": "no-cache",
            "Cache-Control": "no-cache",
            "Accept": "application/json, text/plain, */*",
            "Origin": "http://www.nxtbus.act.gov.au",
            "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36",
            "Content-Type": "application/json;charset=UTF-8",
            "Referer": "http://www.nxtbus.act.gov.au/",
            "Accept-Encoding": "gzip, deflate",
            "Accept-Language": "en-US,en;q=0.9,zh-CN;q=0.8,zh;q=0.7,zh-TW;q=0.6",
            "Cookie": "_ga=GA1.3.1047350369.1520330486; JSESSIONID=A9867939633BBCDF7376DD0F478385D1; _gid=GA1.3.418423057.1523441031"

        }


        # Adding empty header as parameters are being sent in payload
        payload = {
            "stopId": self.stop_id,
            "stopType": "BUS_STOP",
            "departureDate": current_time,
            "departureTime": current_time,
            "departureOrArrival": "DEPARTURE",
            "requestTime": current_time
        }

        r = pyRequest.post(url, data=json.dumps(payload), headers=headers)
        return r.json()